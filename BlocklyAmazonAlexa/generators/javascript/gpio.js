/**
 * @license
 * Visual Blocks Language
 *
 * Copyright 2014 Google Inc.
 * https://developers.google.com/blockly/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @fileoverview Generating c for variable blocks.
 * @author fraser@google.com (Neil Fraser)
 */
'use strict';

goog.provide('Blockly.JavaScript.gpio');
goog.require('Blockly.JavaScript');

Blockly.JavaScript['gpio_digital_get'] = function(block) {
  // Variable getter.
  var code = Blockly.JavaScript.variableDB_.getName(block.getFieldValue('VAR'),
      Blockly.Variables.NAME_TYPE);
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['gpio_digital_set'] = function(block) {
  var argument0 = Blockly.JavaScript.valueToCode(block, 'VALUE',
      Blockly.JavaScript.ORDER_ASSIGNMENT) || '0';
  var varName = Blockly.JavaScript.variableDB_.getName(block.getFieldValue('VAR'),
      Blockly.Variables.NAME_TYPE);
  return  varName + ' = ' + argument0 + ';\n';
};

/*
function sleep(ms){

var starttime= new Date().getTime();

do{

}while((new Date().getTime()-starttime)<ms)

}
*/
Blockly.JavaScript['gpio_delay'] = function(block) {
	
var Lan=Blockly.JavaScript;
  var argument0 =Lan.valueToCode(block, 'VALUE',
       Lan.ORDER_ASSIGNMENT) || '0';
  // return 'delay(' + argument0 + ');\n'; 
  
  var str="var icblockvalgpio_delay= new Date().getTime();\ndo{\n}while((new Date().getTime()-icblockvalgpio_delay)<%1)\n";
  
  str=str.replace(/%1/, argument0);
  return str;



};

Blockly.JavaScript['gpio_pin_init'] = function(block) {
var Lan=Blockly.JavaScript;
//  var code = (block.getFieldValue('VALUE') == 'TRUE') ? 'true' : 'false';
  var code2 = block.getFieldValue('VALUE');
  var argument0 = Lan.valueToCode(block, 'VALUE',
      Lan.ORDER_ASSIGNMENT) || '0';
  var varName = Lan.variableDB_.getName(block.getFieldValue('VAR'),
     Blockly.Variables.NAME_TYPE);
   if(code2=="Digital Output"){
	  return 'nrf_gpio_cfg_output('+varName+');\n';
   }else  if(code2=="Digital Input"){
	  return 'nrf_gpio_cfg_input('+varName +');\n';
   }
  return 'PinInit('+'1'+code+'2'+code2+'3'+varName+', ' + argument0 + ');\n';
  
};


