/**
 * @license
 * Visual Blocks Language
 *
 * Copyright 2014 Google Inc.
 * https://developers.google.AmazonAlexaom/blockly/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @fileoverview Generating c for variable blocks.
 * @author fraser@google.com (Neil Fraser)
 */
'use strict';

goog.provide('Blockly.AmazonAlexa.utterances');

goog.require('Blockly.AmazonAlexa');

/*

OneshotTideIntent get high tide
OneshotTideIntent get high tide for {City} {State}
OneshotTideIntent get high tide for {City} {State} {Date}
OneshotTideIntent get high tide for {City} {Date}
OneshotTideIntent get high tide for {Date}
OneshotTideIntent get high tide {Date}

*/



Blockly.AmazonAlexa['utterances_OneshotTideIntent_get_high_tide'] = function(block) {
  // Create a list with any number of elements of any type.
  var isState=false;
  var isDate=false;
  var isCity=false;

  var Str1="OneshotTideIntent get high tide";
  var StrReturn=Str1+"\r\n";
  var code = new Array(block.itemCount_);
  for (var n = 0; n < block.itemCount_; n++) {
    code[n] = Blockly.AmazonAlexa.valueToCode(block, 'ADD' + n,
        Blockly.AmazonAlexa.ORDER_NONE) || 'null';
    if(code[n]=="State"){
       StrReturn=StrReturn+Str1+" {"+code[n]+"}\r\n";
       isState=true;
    }
     if(code[n]=="Date"){
       StrReturn=StrReturn+Str1+" {"+code[n]+"}\r\n";
       isDate=true;
    }
     if(code[n]=="City"){
        StrReturn=StrReturn+Str1+" {"+code[n]+"}\r\n";
        isCity=true;
    }
  }
  if(isCity && isState){
        StrReturn=StrReturn+Str1+" {City} {State}\r\n";
  }
  if(isCity && isDate){
        StrReturn=StrReturn+Str1+" {City} {Date}\r\n";
  }
  if(isCity && isDate  && isState){
        StrReturn=StrReturn+Str1+" {City} {Date} {State}\r\n";
  }
    return  StrReturn;
};



Blockly.AmazonAlexa['utterances_OneshotTideIntent_get_tide_information'] = function(block) {
  // Create a list with any number of elements of any type.
  var isState=false;
  var isDate=false;
  var isCity=false;

  var Str1="OneshotTideIntent get tide information";
  var StrReturn=Str1+"\r\n";
  var code = new Array(block.itemCount_);
  for (var n = 0; n < block.itemCount_; n++) {
    code[n] = Blockly.AmazonAlexa.valueToCode(block, 'ADD' + n,
        Blockly.AmazonAlexa.ORDER_NONE) || 'null';
    if(code[n]=="State"){
       StrReturn=StrReturn+Str1+" {"+code[n]+"}\r\n";
       isState=true;
    }
     if(code[n]=="Date"){
       StrReturn=StrReturn+Str1+" {"+code[n]+"}\r\n";
       isDate=true;
    }
     if(code[n]=="City"){
        StrReturn=StrReturn+Str1+" {"+code[n]+"}\r\n";
        isCity=true;
    }
  }
  if(isCity && isState){
        StrReturn=StrReturn+Str1+" {City} {State}\r\n";
  }
  if(isCity && isDate){
        StrReturn=StrReturn+Str1+" {City} {Date}\r\n";
  }
  if(isCity && isDate  && isState){
        StrReturn=StrReturn+Str1+" {City} {Date} {State}\r\n";
  }
    return  StrReturn;
};



Blockly.AmazonAlexa['utterances_OneshotTideIntent_get_tides_for'] = function(block) {
  // Create a list with any number of elements of any type.
  var isState=false;
  var isDate=false;
  var isCity=false;

  var Str1="OneshotTideIntent get tides for";
  var StrReturn=Str1+"\r\n";
  var code = new Array(block.itemCount_);
  for (var n = 0; n < block.itemCount_; n++) {
    code[n] = Blockly.AmazonAlexa.valueToCode(block, 'ADD' + n,
        Blockly.AmazonAlexa.ORDER_NONE) || 'null';
    if(code[n]=="State"){
       StrReturn=StrReturn+Str1+" {"+code[n]+"}\r\n";
       isState=true;
    }
     if(code[n]=="Date"){
       StrReturn=StrReturn+Str1+" {"+code[n]+"}\r\n";
       isDate=true;
    }
     if(code[n]=="City"){
        StrReturn=StrReturn+Str1+" {"+code[n]+"}\r\n";
        isCity=true;
    }
  }
  if(isCity && isState){
        StrReturn=StrReturn+Str1+" {City} {State}\r\n";
  }
  if(isCity && isDate){
        StrReturn=StrReturn+Str1+" {City} {Date}\r\n";
  }
  if(isCity && isDate  && isState){
        StrReturn=StrReturn+Str1+" {City} {Date} {State}\r\n";
  }
    return  StrReturn;
};



Blockly.AmazonAlexa['utterances_OneshotTideIntent_when_is_high_tide_for'] = function(block) {
  // Create a list with any number of elements of any type.
  var isState=false;
  var isDate=false;
  var isCity=false;

  var Str1="OneshotTideIntent when is high tide for";
  var StrReturn=Str1+"\r\n";
  var code = new Array(block.itemCount_);
  for (var n = 0; n < block.itemCount_; n++) {
    code[n] = Blockly.AmazonAlexa.valueToCode(block, 'ADD' + n,
        Blockly.AmazonAlexa.ORDER_NONE) || 'null';
    if(code[n]=="State"){
       StrReturn=StrReturn+Str1+" {"+code[n]+"}\r\n";
       isState=true;
    }
     if(code[n]=="Date"){
       StrReturn=StrReturn+Str1+" {"+code[n]+"}\r\n";
       isDate=true;
    }
     if(code[n]=="City"){
        StrReturn=StrReturn+Str1+" {"+code[n]+"}\r\n";
        isCity=true;
    }
  }
  if(isCity && isState){
        StrReturn=StrReturn+Str1+" {City} {State}\r\n";
  }
  if(isCity && isDate){
        StrReturn=StrReturn+Str1+" {City} {Date}\r\n";
  }
  if(isCity && isDate  && isState){
        StrReturn=StrReturn+Str1+" {City} {Date} {State}\r\n";
  }
    return  StrReturn;
};



Blockly.AmazonAlexa['utterances_OneshotTideIntent_when_is_next_tide_in'] = function(block) {
  // Create a list with any number of elements of any type.
  var isState=false;
  var isDate=false;
  var isCity=false;

  var Str1="OneshotTideIntent when is next tide in";
  var StrReturn=Str1+"\r\n";
  var code = new Array(block.itemCount_);
  for (var n = 0; n < block.itemCount_; n++) {
    code[n] = Blockly.AmazonAlexa.valueToCode(block, 'ADD' + n,
        Blockly.AmazonAlexa.ORDER_NONE) || 'null';
    if(code[n]=="State"){
       StrReturn=StrReturn+Str1+" {"+code[n]+"}\r\n";
       isState=true;
    }
     if(code[n]=="Date"){
       StrReturn=StrReturn+Str1+" {"+code[n]+"}\r\n";
       isDate=true;
    }
     if(code[n]=="City"){
        StrReturn=StrReturn+Str1+" {"+code[n]+"}\r\n";
        isCity=true;
    }
  }
  if(isCity && isState){
        StrReturn=StrReturn+Str1+" {City} {State}\r\n";
  }
  if(isCity && isDate){
        StrReturn=StrReturn+Str1+" {City} {Date}\r\n";
  }
  if(isCity && isDate  && isState){
        StrReturn=StrReturn+Str1+" {City} {Date} {State}\r\n";
  }
    return  StrReturn;
};



Blockly.AmazonAlexa['utterances_OneshotTideIntent_when_is_the_next_highest_water'] = function(block) {
  // Create a list with any number of elements of any type.
  var isState=false;
  var isDate=false;
  var isCity=false;

  var Str1="OneshotTideIntent when is the next highest water";
  var StrReturn=Str1+"\r\n";
  var code = new Array(block.itemCount_);
  for (var n = 0; n < block.itemCount_; n++) {
    code[n] = Blockly.AmazonAlexa.valueToCode(block, 'ADD' + n,
        Blockly.AmazonAlexa.ORDER_NONE) || 'null';
    if(code[n]=="State"){
       StrReturn=StrReturn+Str1+" {"+code[n]+"}\r\n";
       isState=true;
    }
     if(code[n]=="Date"){
       StrReturn=StrReturn+Str1+" {"+code[n]+"}\r\n";
       isDate=true;
    }
     if(code[n]=="City"){
        StrReturn=StrReturn+Str1+" {"+code[n]+"}\r\n";
        isCity=true;
    }
  }
  if(isCity && isState){
        StrReturn=StrReturn+Str1+" {City} {State}\r\n";
  }
  if(isCity && isDate){
        StrReturn=StrReturn+Str1+" {City} {Date}\r\n";
  }
  if(isCity && isDate  && isState){
        StrReturn=StrReturn+Str1+" {City} {Date} {State}\r\n";
  }
    return  StrReturn;
};




Blockly.AmazonAlexa['utterances_OneshotTideIntent_when_is_the_next_tide_for'] = function(block) {
  // Create a list with any number of elements of any type.
  var isState=false;
  var isDate=false;
  var isCity=false;

  var Str1="OneshotTideIntent when is the next tide for";
  var StrReturn=Str1+"\r\n";
  var code = new Array(block.itemCount_);
  for (var n = 0; n < block.itemCount_; n++) {
    code[n] = Blockly.AmazonAlexa.valueToCode(block, 'ADD' + n,
        Blockly.AmazonAlexa.ORDER_NONE) || 'null';
    if(code[n]=="State"){
       StrReturn=StrReturn+Str1+" {"+code[n]+"}\r\n";
       isState=true;
    }
     if(code[n]=="Date"){
       StrReturn=StrReturn+Str1+" {"+code[n]+"}\r\n";
       isDate=true;
    }
     if(code[n]=="City"){
        StrReturn=StrReturn+Str1+" {"+code[n]+"}\r\n";
        isCity=true;
    }
  }
  if(isCity && isState){
        StrReturn=StrReturn+Str1+" {City} {State}\r\n";
  }
  if(isCity && isDate){
        StrReturn=StrReturn+Str1+" {City} {Date}\r\n";
  }
  if(isCity && isDate  && isState){
        StrReturn=StrReturn+Str1+" {City} {Date} {State}\r\n";
  }
    return  StrReturn;
};

Blockly.AmazonAlexa['utterances_OneshotTideIntent_when_will_the_water_be_highest_for'] = function(block) {
  // Create a list with any number of elements of any type.
  var isState=false;
  var isDate=false;
  var isCity=false;

  var Str1="OneshotTideIntent when will the water be highest for";
  var StrReturn=Str1+"\r\n";
  var code = new Array(block.itemCount_);
  for (var n = 0; n < block.itemCount_; n++) {
    code[n] = Blockly.AmazonAlexa.valueToCode(block, 'ADD' + n,
        Blockly.AmazonAlexa.ORDER_NONE) || 'null';
    if(code[n]=="State"){
       StrReturn=StrReturn+Str1+" {"+code[n]+"}\r\n";
       isState=true;
    }
     if(code[n]=="Date"){
       StrReturn=StrReturn+Str1+" {"+code[n]+"}\r\n";
       isDate=true;
    }
     if(code[n]=="City"){
        StrReturn=StrReturn+Str1+" {"+code[n]+"}\r\n";
        isCity=true;
    }
  }
  if(isCity && isState){
        StrReturn=StrReturn+Str1+" {City} {State}\r\n";
  }
  if(isCity && isDate){
        StrReturn=StrReturn+Str1+" {City} {Date}\r\n";
  }
  if(isCity && isDate  && isState){
        StrReturn=StrReturn+Str1+" {City} {Date} {State}\r\n";
  }
    return  StrReturn;
};
