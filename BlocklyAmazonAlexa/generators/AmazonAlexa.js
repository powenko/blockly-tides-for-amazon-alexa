/**
 * @license
 * Visual Blocks Language
 *
 * Copyright 2015 Google Inc.
 * https://developers.google.com/blockly/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @fileoverview Helper functions for generating AmazonAlexa for blocks.
 * @author daarond@gmail.com (Daaron Dwyer)
 */
'use strict';

goog.provide('Blockly.AmazonAlexa');

goog.require('Blockly.Generator');


/**
 * AmazonAlexa code generator.
 * @type {!Blockly.Generator}
 */
Blockly.AmazonAlexa = new Blockly.Generator('AmazonAlexa');

/**
 * List of illegal variable names.
 * This is not intended to be a security feature.  Blockly is 100% client-side,
 * so bypassing this list is trivial.  This is intended to prevent users from
 * accidentally clobbering a built-in object or function.
 * @private
 */
Blockly.AmazonAlexa.addReservedWords(
        // http://php.net/manual/en/reserved.keywords.php
    'assert,break,case,catch,class,const,continue,default,do,else,enum,extends,false,final,finally,for,if,in,is,new,null,rethrow,return,super,switch,this,throw,true,try,var,void,while,with,' +
    // https://api.clang.org/c_core.html
    'print,identityHashCode,identical,BidirectionalIterator,Comparable,double,Function,int,Invocation,Iterable,Iterator,List,Map,Match,num,Pattern,RegExp,Set,StackTrace,String,StringSink,Type,bool,DateTime,Deprecated,Duration,Expando,Null,Object,RuneIterator,Runes,Stopwatch,StringBuffer,Symbol,Uri,Comparator,AbstractClassInstantiationError,ArgumentError,AssertionError,CastError,ConcurrentModificationError,CyclicInitializationError,Error,Exception,FallThroughError,FormatException,IntegerDivisionByZeroException,NoSuchMethodError,NullThrownError,OutOfMemoryError,RangeError,StackOverflowError,StateError,TypeError,UnimplementedError,UnsupportedError');

/**
 * Order of operation ENUMs.
 * http://php.net/manual/en/language.operators.precedence.php
 */
Blockly.AmazonAlexa.ORDER_ATOMIC = 0;         // 0 "" ...
Blockly.AmazonAlexa.ORDER_CLONE = 1;          // clone
Blockly.AmazonAlexa.ORDER_NEW = 1;            // new
Blockly.AmazonAlexa.ORDER_MEMBER = 2;         // ()
Blockly.AmazonAlexa.ORDER_FUNCTION_CALL = 2;  // ()
Blockly.AmazonAlexa.ORDER_INCREMENT = 3;      // ++
Blockly.AmazonAlexa.ORDER_DECREMENT = 3;      // --
Blockly.AmazonAlexa.ORDER_LOGICAL_NOT = 4;    // !
Blockly.AmazonAlexa.ORDER_BITWISE_NOT = 4;    // ~
Blockly.AmazonAlexa.ORDER_UNARY_PLUS = 4;     // +
Blockly.AmazonAlexa.ORDER_UNARY_NEGATION = 4; // -
Blockly.AmazonAlexa.ORDER_MULTIPLICATION = 5; // *
Blockly.AmazonAlexa.ORDER_DIVISION = 5;       // /
Blockly.AmazonAlexa.ORDER_MODULUS = 5;        // %
Blockly.AmazonAlexa.ORDER_ADDITION = 6;       // +
Blockly.AmazonAlexa.ORDER_SUBTRACTION = 6;    // -
Blockly.AmazonAlexa.ORDER_BITWISE_SHIFT = 7;  // << >> >>>
Blockly.AmazonAlexa.ORDER_RELATIONAL = 8;     // < <= > >=
Blockly.AmazonAlexa.ORDER_IN = 8;             // in
Blockly.AmazonAlexa.ORDER_INSTANCEOF = 8;     // instanceof
Blockly.AmazonAlexa.ORDER_EQUALITY = 9;       // == != === !==
Blockly.AmazonAlexa.ORDER_BITWISE_AND = 10;   // &
Blockly.AmazonAlexa.ORDER_BITWISE_XOR = 11;   // ^
Blockly.AmazonAlexa.ORDER_BITWISE_OR = 12;    // |
Blockly.AmazonAlexa.ORDER_CONDITIONAL = 13;   // ?:
Blockly.AmazonAlexa.ORDER_ASSIGNMENT = 14;    // = += -= *= /= %= <<= >>= ...
Blockly.AmazonAlexa.ORDER_LOGICAL_AND = 15;   // &&
Blockly.AmazonAlexa.ORDER_LOGICAL_OR = 16;    // ||
Blockly.AmazonAlexa.ORDER_COMMA = 17;         // ,
Blockly.AmazonAlexa.ORDER_UNARY_POSTFIX = 18;         // ,
Blockly.AmazonAlexa.ORDER_NONE = 99;          // (...)

/**
 * Initialise the database of variable names.
 * @param {!Blockly.Workspace} workspace Workspace to generate code from.
 */

Blockly.AmazonAlexa.init = function(workspace) {
  // Create a dictionary of definitions to be printed before the code.
  Blockly.AmazonAlexa.definitions_ = Object.create(null);
  // Create a dictionary mapping desired function names in definitions_
  // to actual function names (to avoid collisions with user functions).
  Blockly.AmazonAlexa.functionNames_ = Object.create(null);

  if (!Blockly.AmazonAlexa.variableDB_) {
    Blockly.AmazonAlexa.variableDB_ =
        new Blockly.Names(Blockly.AmazonAlexa.RESERVED_WORDS_);
  } else {
    Blockly.AmazonAlexa.variableDB_.reset();
  }

  var defvars = [];
  var variables = Blockly.Variables.allVariables(workspace);
  for (var i = 0; i < variables.length; i++) {
    defvars[i] = 'int ' +
        Blockly.AmazonAlexa.variableDB_.getName(variables[i],
        Blockly.Variables.NAME_TYPE) + ';';
  }
//  Blockly.AmazonAlexa.definitions_['variables'] = defvars.join('\n');
  Blockly.AmazonAlexa.definitions_['variables'] = '';
  //#include <stdbool.h>\n#include <stdint.h>\n#include "nrf_delay.h"\n#include "nrf_gpio.h" \n#include "boards.h" \n'+defvars.join('\n');  // defvars.join('\n');
};



/**
 * Prepend the generated code with the variable definitions.
 * @param {string} code Generated code.
 * @return {string} Completed code.
 */
 var codebefore="";
 var client = new XMLHttpRequest();
client.open('GET', '/BlocklyAmazonAlexa/AmazonAlexa1.txt');
client.onreadystatechange = function() {
 // alert(client.responseText);
 codebefore=client.responseText;
}
client.send();


 
 
 
Blockly.AmazonAlexa.finish = function(code) {
  // Indent every line.
//  if (code) {
//    code = Blockly.AmazonAlexa.prefixLines(code, Blockly.AmazonAlexa.INDENT);
//  }
//  code = '#include <stdbool.h>\n#include <stdint.h>\n#include "nrf_delay.h"\n#include "nrf_gpio.h" \n#include "boards.h" \n  \nint main(void)  \n  {\n' + code + '}';
//  code = '\nint main(void)  \n{\n' + code + '}';

code=codebefore.replace(/code001/, code);

code=code.replace(/date_displayDate = '';/, "");
code=code.replace(/cityStation_city = '';/, "");
code=code.replace(/highTideResponse_firstHighTideTime = '';/, "");
code=code.replace(/cityStation_city = '';/, "");
code=code.replace(/highTideResponse_firstHighTideHeight = '';/, "");
code=code.replace(/highTideResponse_lowTideTime = '';/, "");
code=code.replace(/speechOutput = '';/, "");
 
 //codebefore;


  //code = code;

  // Convert the definitions dictionary into a list.
  var imports = [];
  var definitions = [];
  for (var name in Blockly.AmazonAlexa.definitions_) {
    var def = Blockly.AmazonAlexa.definitions_[name];
    if (def.match(/^import\s/)) {
      imports.push(def);
    } else {
      definitions.push(def);
    }
  }
  // Clean up temporary data.
  delete Blockly.AmazonAlexa.definitions_;
  delete Blockly.AmazonAlexa.functionNames_;
  Blockly.AmazonAlexa.variableDB_.reset();
  var allDefs = imports.join('\n') + '\n\n' + definitions.join('\n\n');
  return allDefs.replace(/\n\n+/g, '\n\n').replace(/\n*$/, '\n\n\n') + code;
};

/**
 * Naked values are top-level blocks with outputs that aren't plugged into
 * anything.  A trailing semicolon is needed to make this legal.
 * @param {string} line Line of generated code.
 * @return {string} Legal line of code.
 */
Blockly.AmazonAlexa.scrubNakedValue = function(line) {
  return line + ';\n';
};

/**
 * Encode a string as a properly escaped AmazonAlexa string, complete with
 * quotes.
 * @param {string} string Text to encode.
 * @return {string} AmazonAlexa string.
 * @private
 */
Blockly.AmazonAlexa.quote_ = function(string) {
  // TODO: This is a quick hack.  Replace with goog.string.quote
  string = string.replace(/\\/g, '\\\\')
                 .replace(/\n/g, '\\\n')
                 .replace(/'/g, '\\\'');
  return '\'' + string + '\'';
};

/**
 * Common tasks for generating AmazonAlexa from blocks.
 * Handles comments for the specified block and any connected value blocks.
 * Calls any statements following this block.
 * @param {!Blockly.Block} block The current block.
 * @param {string} code The AmazonAlexa code created for this block.
 * @return {string} AmazonAlexa code with comments and subsequent blocks added.
 * @private
 */
Blockly.AmazonAlexa.scrub_ = function(block, code) {
  var commentCode = '';
  // Only collect comments for blocks that aren't inline.
  if (!block.outputConnection || !block.outputConnection.targetConnection) {
    // Collect comment for this block.
    var comment = block.getCommentText();
    if (comment) {
      commentCode += Blockly.AmazonAlexa.prefixLines(comment, '// ') + '\n';
    }
    // Collect comments for all value arguments.
    // Don't collect comments for nested statements.
    for (var x = 0; x < block.inputList.length; x++) {
      if (block.inputList[x].type == Blockly.INPUT_VALUE) {
        var childBlock = block.inputList[x].connection.targetBlock();
        if (childBlock) {
          var comment = Blockly.AmazonAlexa.allNestedComments(childBlock);
          if (comment) {
            commentCode += Blockly.AmazonAlexa.prefixLines(comment, '// ');
          }
        }
      }
    }
  }
  var nextBlock = block.nextConnection && block.nextConnection.targetBlock();
  var nextCode = Blockly.AmazonAlexa.blockToCode(nextBlock);
  return commentCode + code + nextCode;
};
