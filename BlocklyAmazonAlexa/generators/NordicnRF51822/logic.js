/**
 * @license
 * Visual Blocks Language
 *
 * Copyright 2014 Google Inc.
 * https://developers.google.com/blockly/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @fileoverview Generating c for logic blocks.
 * @author q.neutron@gmail.com (Quynh Neutron)
 */
'use strict';

goog.provide('Blockly.NordicnRF51822.logic');

goog.require('Blockly.NordicnRF51822');


Blockly.NordicnRF51822['controls_if'] = function(block) {
  // If/elseif/else condition.
  var n = 0;
  var argument = Blockly.NordicnRF51822.valueToCode(block, 'IF' + n,
      Blockly.NordicnRF51822.ORDER_NONE) || 'false';
  var branch = Blockly.NordicnRF51822.statementToCode(block, 'DO' + n);
  var code = 'if (' + argument + ') {\n' + branch + '}';
  for (n = 1; n <= block.elseifCount_; n++) {
    argument = Blockly.NordicnRF51822.valueToCode(block, 'IF' + n,
      Blockly.NordicnRF51822.ORDER_NONE) || 'false';
    branch = Blockly.NordicnRF51822.statementToCode(block, 'DO' + n);
    code += ' else if (' + argument + ') {\n' + branch + '}';
  }
  if (block.elseCount_) {
    branch = Blockly.NordicnRF51822.statementToCode(block, 'ELSE');
    code += ' else {\n' + branch + '}';
  }
  return code + '\n';
};

Blockly.NordicnRF51822['logic_compare'] = function(block) {
  // Comparison operator.
  var OPERATORS = {
    'EQ': '==',
    'NEQ': '!=',
    'LT': '<',
    'LTE': '<=',
    'GT': '>',
    'GTE': '>='
  };
  var operator = OPERATORS[block.getFieldValue('OP')];
  var order = (operator == '==' || operator == '!=') ?
      Blockly.NordicnRF51822.ORDER_EQUALITY : Blockly.NordicnRF51822.ORDER_RELATIONAL;
  var argument0 = Blockly.NordicnRF51822.valueToCode(block, 'A', order) || '0';
  var argument1 = Blockly.NordicnRF51822.valueToCode(block, 'B', order) || '0';
  var code = argument0 + ' ' + operator + ' ' + argument1;
  return [code, order];
};

Blockly.NordicnRF51822['logic_operation'] = function(block) {
  // Operations 'and', 'or'.
  var operator = (block.getFieldValue('OP') == 'AND') ? '&&' : '||';
  var order = (operator == '&&') ? Blockly.NordicnRF51822.ORDER_LOGICAL_AND :
      Blockly.NordicnRF51822.ORDER_LOGICAL_OR;
  var argument0 = Blockly.NordicnRF51822.valueToCode(block, 'A', order);
  var argument1 = Blockly.NordicnRF51822.valueToCode(block, 'B', order);
  if (!argument0 && !argument1) {
    // If there are no arguments, then the return value is false.
    argument0 = 'false';
    argument1 = 'false';
  } else {
    // Single missing arguments have no effect on the return value.
    var defaultArgument = (operator == '&&') ? 'true' : 'false';
    if (!argument0) {
      argument0 = defaultArgument;
    }
    if (!argument1) {
      argument1 = defaultArgument;
    }
  }
  var code = argument0 + ' ' + operator + ' ' + argument1;
  return [code, order];
};

Blockly.NordicnRF51822['logic_negate'] = function(block) {
  // Negation.
  var order = Blockly.NordicnRF51822.ORDER_UNARY_PREFIX;
  var argument0 = Blockly.NordicnRF51822.valueToCode(block, 'BOOL', order) || 'true';
  var code = '!' + argument0;
  return [code, order];
};

Blockly.NordicnRF51822['logic_boolean'] = function(block) {
  // Boolean values true and false.
  var code = (block.getFieldValue('BOOL') == 'TRUE') ? 'true' : 'false';
  return [code, Blockly.NordicnRF51822.ORDER_ATOMIC];
};

Blockly.NordicnRF51822['logic_null'] = function(block) {
  // Null data type.
  return ['null', Blockly.NordicnRF51822.ORDER_ATOMIC];
};

Blockly.NordicnRF51822['logic_ternary'] = function(block) {
  // Ternary operator.
  var value_if = Blockly.NordicnRF51822.valueToCode(block, 'IF',
      Blockly.NordicnRF51822.ORDER_CONDITIONAL) || 'false';
  var value_then = Blockly.NordicnRF51822.valueToCode(block, 'THEN',
      Blockly.NordicnRF51822.ORDER_CONDITIONAL) || 'null';
  var value_else = Blockly.NordicnRF51822.valueToCode(block, 'ELSE',
      Blockly.NordicnRF51822.ORDER_CONDITIONAL) || 'null';
  var code = value_if + ' ? ' + value_then + ' : ' + value_else;
  return [code, Blockly.NordicnRF51822.ORDER_CONDITIONAL];
};
